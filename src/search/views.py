# coding: utf-8


import json
from django.shortcuts import HttpResponse
from django.db.models.query_utils import Q
from django.db.models import Count
from django.contrib.postgres.search import TrigramSimilarity
from django.http import Http404
from digg_paginator import DiggPaginator
from .base import ShopSearchBaseView, ShopSearchParamsValidatorMixin


PAGE_SIZE = 20
PAGE_COUNTER_S = [9, 18, 30, 60, 90]
CATEGORY_GRID_COUNT = 6
MIN_PRICE = 0
MAX_PRICE = 9999999
MIN_STOCK = 0
MAX_STOCK = 9999999


class ShopSearchLookupView(ShopSearchBaseView, ShopSearchParamsValidatorMixin):

    """ Search View. Receives get params
        and response neither arguments in get
        request params.

        GET Params:

        1. AJAX - if ajax is True, we have response
        html part, that insert in DOM structure in client
        side. If we have True, we response all html
        document with base template.
        2. ITEM_S - list of dicts of items with params
        (
            id: id of product/subproduct
        )

        ALL PARAMS put in params_storage after validate
    """

    STRING_LIMIT = 100
    LOOKUP_LIMIT = 10
    PRODUCT_MODEL = None

    request_params_slots = {
        'query': [None, ''],
    }

    def __init__(self, *args, **kwargs):
        self.params_storage = {}
        self.output_context = {
            'lookup_obj_s': None
        }
        super(ShopSearchLookupView, self).__init__(*args, **kwargs)

    def _set_query(self):
        self._query = self.params_storage['query']

    def _set_item_set(self):
        self.item_set = self.PRODUCT_MODEL.objects.annotate(similarity=TrigramSimilarity('title', self._query)).\
            filter(similarity__gt=0.1).order_by('-similarity')[:self.LOOKUP_LIMIT]

    def _set_lookup(self):
        self.lookup_obj_s = \
            [{'pk': p.pk, 'name': '{}'.format(p.title)[:self.STRING_LIMIT]} for p in self.item_set]

    def get(self, *args, **kwargs):
        self._set_query()
        self._set_item_set()
        self._set_lookup()
        self._aggregate()
        return HttpResponse(json.dumps(self.output_context))


class ShopSearchListView(ShopSearchBaseView, ShopSearchParamsValidatorMixin):

    PAGE_COUNT = 21
    PRODUCT_MODEL = None

    PRODUCT_PARAMS_KV_MODEL = None
    PRODUCT_PARAMS_KV_LIMIT = 4
    ORDER_REFERENCE_MODEL = None
    ORDER_REFERENCE_DEFAULT = None
    FILTER_MODEL = None

    FILTER_S = [
        ('FIELD', 'PRICE', {'UNIT': 'р.', 'CODE': 'price', 'FIELD_NAME': 'price'}),
        ('KV', '', {})
    ]

    context_processors = []
    template_popup_change = {}

    request_params_slots = {
        'query': [None, ''],
        'order': [None, None],
        'page': [None, 1],
        'filter': [None, {}],
        'count': [None, PAGE_COUNTER_S[0]],
        'grid': [None, 0]
    }
    
    def __init__(self, *args, **kwargs):
        self.params_storage = {}
        self.output_context = {
            'search_set': None,
            'page': None,
            'query': None
        }
        super(ShopSearchListView, self).__init__(*args, **kwargs)

    def _set_query(self):
        query_full = self.params_storage['query'].split(' - ')
        self.query = query_full[1] if len(query_full) > 1 else query_full[0]

    def _set_item_set(self):
        self.search_set = self.PRODUCT_MODEL.objects.filter(
            Q(title__icontains=self.query) | Q(code__icontains=self.query)
        ).all().order_by('title')

    def _set_item_filter_s(self):
        """

        :return:
        """
        qdata = self.params_storage['filter']
        if not qdata:
            return

        for filter_type, filter_field, filter_opt in self.FILTER_S:
            filter_field = filter_field.lower()
            params = qdata.get(filter_type, {})
            if filter_type == 'FIELD':
                params = params.get(filter_opt['CODE'], {})
                if params.get('from'):
                    self.search_set = self.search_set.filter(
                        **{'{}__gte'.format(filter_field): params.get('from')}
                    )
                if params.get('to'):
                    self.search_set = self.search_set.filter(
                        **{'{}__lte'.format(filter_field): params.get('to')}
                    )
            if filter_type in ['M2M', 'FK']:
                params = params.get(filter_opt['CODE'], {})
                selected = str(params['selected']).split(',') if params.get('selected') else []
                if filter_field:
                    self.search_set = getattr(self, filter_field)(selected=selected)
                elif selected:
                    self.search_set = self.search_set.\
                        filter(**{'{0}__{0}__pk__in'.format(filter_opt['CODE']): [int(i) for i in selected]})
            if filter_type == 'KV':
                kv_codes = self.PRODUCT_PARAMS_KV_MODEL.objects.filter(product__in=self.search_set). \
                               values('code', 'title').annotate(params_count=Count('code')). \
                               order_by('-params_count')[:self.PRODUCT_PARAMS_KV_LIMIT]
                for k in kv_codes:
                    params = params.get(k['code'], {})
                    selected = str(params['selected']).split(',') if params.get('selected') else []
                    selected = [p.replace('{}-'.format(k['code']), '') for p in selected]
                    if selected:
                        self.search_set = self.search_set.filter(**{
                            'params_kv__code': k['code'],
                            'params_kv__value_hash__in': selected
                        })

    def _set_item_s_pagination(self):
        count = self.params_storage['count']
        count = count if count != 'ALL' else None

        if count:
            paginator = DiggPaginator(self.search_set, self.params_storage['count'])
            page = int(self.params_storage['page'] or 1)
            if paginator.num_pages < page:
                raise Http404
            self.page = paginator.page(page)
        else:
            self.page = {'object_list': self.search_set}

    def _set_order_s(self):
        order_param = self.params_storage['order'] or self.ORDER_REFERENCE_DEFAULT
        order_selected = self.ORDER_REFERENCE_MODEL.objects.filter(code=order_param).first()
        if not order_selected:
            return
        order_name = order_selected.field_name \
            if order_selected.field_order else '-{}'.format(order_selected.field_name)
        self.search_set = self.search_set.order_by(order_name)

    def _set_view_template(self):
        if self.popup:
            self.template_popup['grid'] = self.template_popup_change.get(
                'list' if self.params_storage['grid'] else 'grid')

    def get(self, *args, **kwargs):
        self._set_query()
        self._set_item_set()
        self._set_item_filter_s()
        self._set_order_s()
        self._set_item_s_pagination()
        self._set_view_template()
        self._aggregate()
        return self._render()
