# -*- coding: utf-8 -*-

import json
from leon_base.base.views import BaseView, BaseParamsValidatorMixin


class ShopSearchParamsValidatorMixin(BaseParamsValidatorMixin):

    """ Mixin with validators for validate
        request parameters.
    """

    @staticmethod
    def _query_validator(value, default):
        return value or default

    @staticmethod
    def _page_validator(value, default):
        if value:
            return value
        else:
            return default

    @staticmethod
    def _grid_validator(value, default):
        return int(value) if value and int(value) else default

    @staticmethod
    def _grid_cnt_validator(value, default):
        return default

    @staticmethod
    def _order_validator(value, default):
        if value:
            return value
        else:
            return default

    @staticmethod
    def _filter_validator(value, default):
        try:
            return json.loads(value)
        except BaseException as exc:
            return default

    @staticmethod
    def _item_s_validator(value, default):
        return value

    @staticmethod
    def _count_validator(value, default):
        if value:
            return value
        else:
            return default


class ShopSearchBaseView(BaseView):

    """ Class Base for all Basket Class Views
        When request is received, then
    """

    pass
