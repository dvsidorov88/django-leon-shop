# coding: utf-8


from django.db.models import Max, Min
from django.db.models.query_utils import Q
from django.db.models import Count
from leon_base.base.context_processors import BaseContextProcessor
from .base import ShopSearchParamsValidatorMixin


PAGE_SIZE = 20
PAGE_COUNTER_S = [9, 18, 30, 60, 90]
CATEGORY_GRID_COUNT = 6
MIN_PRICE = 0
MAX_PRICE = 9999999
MIN_STOCK = 0
MAX_STOCK = 9999999


class ShopSearchFilterContextProcessor(BaseContextProcessor, ShopSearchParamsValidatorMixin):
    """
        Class for block context processor filter
    """

    request_params_slots = {
        'query': [None, ''],
        'order': [None, None],
        'page': [None, 1],
        'filter': [None, {}],
        'count': [None, PAGE_COUNTER_S[0]]
    }

    CATEGORY_SITE_MODEL = None
    PRODUCT_MODEL = None
    FILTER_PARAMSKV_GROUP_MODEL = None
    PRODUCT_PARAMS_KV_MODEL = None
    PRODUCT_PARAMS_KV_LIMIT = 4
    ORDER_REFERENCE_MODEL = None
    ORDER_REFERENCE_DEFAULT = None
    HASH_NO_DATA = None
    VALUE_NO_DATA = None

    FILTER_S = [
        ('FIELD', 'PRICE', {'UNIT': 'р.', 'CODE': 'price', 'FIELD_NAME': 'price'}),
        ('KV', '', {})
    ]

    def _set_query(self):
        query_full = self.params_storage['query'].split(' - ')
        self.query = query_full[1] if len(query_full) > 1 else query_full[0]

    def _set_item_set(self):
        self.search_set = self.PRODUCT_MODEL.objects.filter(
            Q(title__icontains=self.query) | Q(code__icontains=self.query)
        ).all().order_by('title')

    def _filter_query_s(self):
        """

        :return:
        """
        qdata = self.params_storage['filter']

        for filter_type, filter_field, filter_opt in self.FILTER_S:
            params = qdata.get(filter_type, {})
            if filter_type == 'FIELD':
                params = params.get(filter_opt['CODE'], {})
                filter_input = {'filter': {
                    'type': filter_type,
                    'code': filter_opt['CODE'],
                    'filed_name': filter_opt['FIELD_NAME']
                }}
                filter_field = filter_field.lower()
                filter_input['max'] = int((self.search_set.aggregate(Max(filter_field))
                                           ['{}__max'.format(filter_field)] or 5) + 2)
                filter_input['min'] = int((self.search_set.aggregate(Min(filter_field))
                                           ['{}__min'.format(filter_field)] or 5) - 2)
                filter_input['from'] = params.get('from') or (filter_input['min'] + 1)
                filter_input['to'] = params.get('to') or (filter_input['max'] - 1)
                filter_input['unit'] = filter_opt['UNIT']
                self.filter_s.append(filter_input)
            if filter_type in ['M2M', 'FK']:
                params = params.get(filter_opt['CODE'], {})
                filter_input = {'filter': {
                    'type': filter_type
                }}
                selected = str(params['selected']).split(',') if params.get('selected') else []
                obj_s = set(self.search_set.values_list(
                    '{0}__{0}__pk'.format(filter_opt['CODE']),
                    '{0}__{0}__title'.format(filter_opt['CODE'])))
                obj_s.remove((None, None)) if (None, None) in obj_s else None
                filter_input['item_s'] = [{'pk': k, 'title': v} for k, v in obj_s]
                filter_input['selected'] = [int(i) for i in selected] if selected \
                    else [item['pk'] for item in filter_input['item_s']]
                self.filter_s.append(filter_input)
            if filter_type == 'KV':
                kv_qs = self.PRODUCT_PARAMS_KV_MODEL.objects.filter(product__in=self.search_set).\
                    values('code', 'title').annotate(params_count=Count('code')). \
                    order_by('-params_count')[:self.PRODUCT_PARAMS_KV_LIMIT]
                for q in kv_qs:
                    filter_input = {'filter': {
                        'type': filter_type,
                        'code': q['code']
                    }}
                    params = params.get(q['code'], {})
                    selected = str(params['selected']).split(',') if params.get('selected') else []

                    values = self.PRODUCT_PARAMS_KV_MODEL.objects.\
                        filter(product__in=self.search_set, code=q['code']).\
                        values('value_hash', 'value').\
                        distinct()

                    filter_input['filter'].update({'title': q['title']})
                    filter_input['item_s'] = [{'pk': '{}-{}'.format(q['code'], p['value_hash']),
                                               'title': p['value']} for p in values] + \
                                             [{'pk': '{}-{}'.format(q['code'], self.HASH_NO_DATA),
                                               'title': self.VALUE_NO_DATA}]
                    filter_input['selected'] = selected
                    self.filter_s.append(filter_input)

    def _order_query_s(self):
        self.order = {}
        order_param = self.params_storage['order']
        self.order['order_s'] = self.ORDER_REFERENCE_MODEL.objects.order_by('-position').all()
        self.order['selected'] = order_param \
            if order_param else self.ORDER_REFERENCE_DEFAULT

    def _counter_query_s(self):
        self.counter = {}
        count_param = self.params_storage['count']
        self.counter['count_s'] = [{'code': i, 'title': i} for i in PAGE_COUNTER_S]
        self.counter['selected'] = count_param if count_param else 'ALL'

    def __call__(self, request):
        self.filter_s = []
        self.output_context = {
            'filter_s': None,
            'order': None,
            'counter': None,
            'search_set': None
        }
        self._init(request)

        self._set_query()
        self._set_item_set()
        self._filter_query_s()

        self._order_query_s()
        self._counter_query_s()
        self._aggregate()
        return self.output_context
